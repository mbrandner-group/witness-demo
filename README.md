# witness-demo

Demo showing a basic integration of witness with a Gitlab CI/CD pipeline.

Tha main branch of the project shows only verification of attestations "existing"

- The policy-fail branch shows the policy of the GCP project ID failing
- The policy pass branch shows an example of the policy being satisfied.

